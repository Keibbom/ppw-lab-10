from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from webservice.models import Account

# Create your views here.
def index(request):
    return render(request, 'index.html', {})

def register_user(request):
	if request.method == 'POST':
		email = request.POST['email']
		name = request.POST['name']
		password = request.POST['password']

		Account.objects.create(
			email = email,
			name = name,
			password = password
		)

		return HttpResponse('')

def check_email(request):
    emailIsExist = False
    if 'email' in request.POST:
        count = Account.objects.filter(email = request.POST['email']).count()
        if count > 0: emailIsExist = True
    return JsonResponse({"email_is_exist": emailIsExist}, content_type = 'application/json')
