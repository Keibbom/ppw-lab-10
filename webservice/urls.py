from django.urls import path, include
from django.conf.urls import url
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('index/', views.index, name='index'),
    url(r'^regist/$', views.register_user),
    url(r'^email_check/$', views.check_email),
]